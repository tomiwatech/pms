var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var PlantSchema = new Schema({
    plant_name: String,
    plant_disease_name: String,
    plant_disease_sympton: String,
    plant_disease_image: String,
    plant_disease_description: String
},
    {
        timestamps: { createdAt: 'created', updatedAt: 'updated' }
    });

var Plant = mongoose.model('Plant', PlantSchema);

module.exports = {
    'Plant': Plant
}
