var app = angular.module('Asset', ['ui.router', 'ngFileUpload']);

app.config(function ($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('login', {
            url: '/login',
            views: {
                layout: {
                    templateUrl: "templates/login.html"
                }
            },
            controller: 'loginController'
        })

        .state('signup', {
            url: '/signup',
            views: {
                layout: {
                    templateUrl: "templates/signup.html"
                }
            },
            controller: 'signupController'
        })

        .state('home', {
            url: '/home',
            views: {
                layout: {
                    templateUrl: "templates/home.html"
                }
            },
            controller: 'homeController'
        })

        .state('addplant', {
            url: '/addplant',
            views: {
                layout: {
                    templateUrl: "templates/addplant.html"
                }
            },
            controller: 'plantController',
            // onEnter: function () {
            //     location.reload();
            // }
        })

        .state('allplants', {
            url: '/allplants',
            views: {
                layout: {
                    templateUrl: "templates/allplants.html"
                }
            },
            controller: 'plantController',
        })

        .state('edit', {
            url: '/edit',
            views: {
                layout: {
                    templateUrl: "templates/edit.html"
                }
            },
            controller: 'plantController',
            params: {
                obj: null
            }
        })

        .state('maintain', {
            url: '/maintain',
            views: {
                layout: {
                    templateUrl: "templates/maintain.html"
                }
            },
            controller: 'plantController',
        })

        .state('document', {
            url: '/document',
            views: {
                layout: {
                    templateUrl: "templates/document.html"
                }
            },
            controller: 'documentController',
        })


    $urlRouterProvider.otherwise('login');
});

app.controller('signupController', function ($scope, $rootScope, $http, $state) {

    function emptyFormFields() {
        $rootScope.data.username == "";
        $rootScope.data.password == "";
        $rootScope.data.email == "";
        $rootScope.data.fullname == "";
    }

    $rootScope.data = {
        "username": "",
        "fullname": "",
        "email": "",
        "password": ""
    }

    $scope.signup = function () {

        if ($rootScope.data.email == "" || $rootScope.data.password == "" || $rootScope.data.fullname == "" || $rootScope.data.username == "") {
            swal('Please fill all fields');
            return;
        }

        var data = {
            'username': $rootScope.data.username,
            'fullname': $rootScope.data.fullname,
            'email': $rootScope.data.email,
            'password': $rootScope.data.password
        }

        $http.post('/users', data).then(function (response) {
            if (response.data.responseCode == "00") {
                swal('Thank You for Signing up. You can now login');
                emptyFormFields();
                $state.go('login');
            } else if (response.data.responseCode == "02") {
                swal('User already exists');
                emptyFormFields();
            } else {
                swal('Error adding user');
                emptyFormFields();
            }

            console.log(JSON.stringify(response));
        }, function (err) {
            console.log(JSON.stringify(err));
        })



    }

})

app.controller('loginController', function ($scope, $rootScope, $http, $state) {

    function emptyFormFields() {
        $rootScope.data.username == "";
        $rootScope.data.password == "";
        $rootScope.data.email == "";
        $rootScope.data.fullname == "";
    }

    $rootScope.data = {
        "username": "",
        "password": ""
    }

    $scope.login = function () {

        if ($rootScope.data.username == "" || $rootScope.data.password == "") {
            swal('Please fill all fields');
            return;
        }

        var datum = {
            'username': $rootScope.data.username,
            'password': $rootScope.data.password
        }

        $http.post('/users/authenticate', datum).then(function (response) {
            // alert(JSON.stringify(response));
            if (response.data.responseCode == "00") {
                $state.go('home');
                emptyFormFields();
            } else if (response.data.responseCode == "02") {
                swal('User Not Found. Pls signup');
                emptyFormFields();
            } else if (response.data.responseCode == "03") {
                swal('Incorrect Password. Pls check password');
                emptyFormFields();
            }
        }, function (err) {
            console.log(JSON.stringify(err));
        });

    }

})

app.controller('plantController', function ($scope, Upload, $rootScope, $http, $state) {

    var refresh = function () {

        $http.get('/plants').then(function (response) {

            $scope.plants = response.data.plants;
            console.log(response)

            $scope.task = {};

        });

        $http.get('/repairs').then(function (response) {

            $scope.repairs = response.data.repair;
            // console.log($scope.repairs);

            $scope.task = {};

        });

    }

    refresh();

    $rootScope.plant = {
        "name": "",
        "disease_name": "",
        "disease_description": "",
        "disease_image": "",
        "disease_sympton": ""
    }

    $scope.addNewPlant = function () {

        if ($rootScope.plant.name == "" || $rootScope.plant.disease_name == "" || $rootScope.plant.disease_description == "" || $rootScope.plant.disease_sympton == "") {
            swal('Please fill all fields');
            return;
        }

        var datum = {
            'plant_name': $rootScope.plant.name,
            'plant_disease_name': $rootScope.plant.disease_name,
            'plant_disease_description': $rootScope.plant.disease_description,
            'plant_disease_sympton': $rootScope.plant.disease_sympton
        }


        if ($scope.form.file.$valid && $scope.file) {
            $scope.upload($scope.file, datum );
        }

        // upload on file select or drop 

    }

    $scope.upload = function (file, data) {
        Upload.upload({
            url: '/plants',
            data: { file: file, data }
        }).then(function (response) {
            if (response.data.responseCode == "00") {
                swal('Your plant details has been saved');
                $state.go('allplants');
            } else if (response.data.responseCode == "02") {
                swal('Plant already exists');
                emptyFormFields();
            } else {
                swal('Error adding Plant');
                emptyFormFields();
            }
        }, function (resp) {
            console.log(resp);
            console.log('Error status: ' + resp.status);
        });
    };

    $scope.deletePlant = function (id) {

        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result === true) {
                console.log(id);
                $http.delete('/plants/' + id).then(function (response) {
                    console.log(response);
                    refresh();
                })
                swal(
                    'Deleted!',
                    'Plant has been deleted.',
                    'success'
                )
            }
        })
    }


    $scope.editPlant = function(id){
       console.log(id);
      
       $http.get('/plants/'+id).then(function(response){

         $rootScope.planter = response.data.result;
         console.log($rootScope.planter);
         $state.go('edit');

       })
    }

    $scope.updatePlant = function(plant){
        console.log(plant);

        var datum = {
            'plant_name': plant.plant_name,
            'plant_disease_name': plant.plant_disease_name,
            'plant_disease_description': plant.plant_disease_description,
            'plant_disease_sympton': plant.plant_disease_sympton,
            '_id' : plant._id,
            'plant_disease_image': plant.plant_disease_image
          
        }

        $http.post('/plants/update', datum).then(function (response) {
            // alert(JSON.stringify(response));
            if (response.data.responseCode == "00") {
                swal('Your plant details have been updated');
                $state.go('allplants');
            } else {
                swal('Error adding Plant');
                emptyFormFields();
            }
        }, function (err) {
            console.log(JSON.stringify(err));
        });
        
    }

  

})

app.controller('homeController', function ($scope, $http ,$state) {

    var refresh = function () {

        $http.get('/plants/count').then(function (response) {

            console.log("response " +JSON.stringify(response))

            if (response.data.responseCode == "02") {
                $scope.total = 0;
               
            } else if (response.data.responseCode == "00") {
                $scope.total = response.data.total;
                console.log(response);
            }

        });

        $http.get('/users/count').then(function (response) {

            $scope.totalAdmin = response.data.total;

        });

        $http.get('/plants').then(function (response) {

            console.log(response)

            $scope.plants = response.data.plants;

        });

    }

    refresh();

    $scope.deletePlant = function (id) {

        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result === true) {
                console.log(id);
                $http.delete('/plants/' + id).then(function (response) {
                    console.log(response);
                    refresh();
                })
                swal(
                    'Deleted!',
                    'Asset has been deleted.',
                    'success'
                )
            }
        })
    }

    // $scope.deezerAPI = function () {

    //     var q = 'eminem';

    //     var data = {
    //         'q': 'asa'
    //     }

    //     $http.get('https://cors-anywhere.herokuapp.com/https://api.deezer.com/search?q=' + q).then(function (response) {

    //         alert(JSON.stringify(response));
    //         console.log(response)

    //     });
    // }

});
