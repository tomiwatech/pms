var express = require('express');
var PlantService = require('../service/plant-service');
var router = express.Router();
var multer = require('multer');
var path = require('path');
var base64Img = require('base64-img');

/* GET Plants listing. */
router.get('/', function (req, res, next) {

    PlantService.allPlants(function (err, plants) {
        if (err) {
            return res.json({
                'responseCode': '03',
                'responseMessage': 'Error fetching Plants'
            });
        }


        if (plants) {
            return res.json({
                'responseCode': '00',
                'responseMessage': 'Successfully fetched Plants',
                'plants': plants
            });
        }

        return res.json({
            'responseCode': '02',
            'responseMessage': 'No Plants in db'
        });
    });
});

router.get('/:id', function (req, res, next) {
    var id = req.params.id;
    console.log(id);
    PlantService.findById(id, function (err, result) {
        if (err) {
            return res.json({
                'responseCode': '03',
                'responseMessage': 'Error fetching plant'
            });
        }

        return res.json({
            'responseCode': '00',
            'responseMessage': 'Successfully fetched plant',
            'result': result
        });
    });
});


router.get('/count', function (req, res, next) {

    PlantService.countAll(function (err, result) {
        if (err) {
            return res.json({
                'responseCode': '03',
                'responseMessage': 'Error fetching Plants total'
            });
        }


        if (result) {
            return res.json({
                'responseCode': '00',
                'responseMessage': 'Successfully fetched Plants total',
                'total': result
            });
        }

        return res.json({
            'responseCode': '02',
            'responseMessage': 'No Plants in db'
        });
    });
});

//Find One
router.post('/authenticate', function (req, res, next) {
    var data = req.body;
    console.log(data.plantname);

    PlantService.findPlant(data.plantname, function (err, plant) {
        if (err) throw err;
        if (!plant) {
            res.json({
                responseCode: "02",
                responseMessage: 'Authentication failed. plant not found.',
            });
        } else if (plant) {
            // check if plantname matches
            if (plant.password != req.body.password) {
                res.json({
                    responseCode: "03",
                    responseMessage: 'Authentication failed. Password not found.',
                });
            } else {
                // if plant is found and password is right
                res.json({
                    responseCode: "00",
                    responseMessage: "Authentication Successful",
                    plant: plant
                });
            }
        }
    })
});

/* POST adds an new plant. */

router.post('/', function (req, res, next) {

    req.res = res;
       // return;
    PlantService.addPlant(req, function (err, plants) {
        if (err) {
            return res.json({
                'responseCode': '03',
                'responseMessage': 'Error adding plant'
            });
        }

        if (plants) {
            return res.json({
                'responseCode': '00',
                'responseMessage': 'Successfully added plant'
            });
        }

        return res.json({
            'responseCode': '02',
            'responseMessage': 'plant exists already'
        });
    });
})



/* POST deletes a plant's record. */
router.delete('/:id', function (req, res, next) {
    var id = req.params.id;
    console.log(id);
    PlantService.deletePlant(id, function (err) {
        if (err) {
            return res.json({
                'responseCode': '03',
                'responseMessage': 'Error deleting plant'
            });
        }

        return res.json({
            'responseCode': '00',
            'responseMessage': 'Successfully deleted plant'
        });
    });
})

// update plant

router.post('/update', function (req, res, next) {

    var data = req.body;

    // console.log(data)
       // return;
    PlantService.updatePlant(data, function (err, plants) {
        if (err) {
            return res.json({
                'responseCode': '03',
                'responseMessage': 'Error updating plant'
            });
        }

        if (plants) {
            return res.json({
                'responseCode': '00',
                'responseMessage': 'Successfully updated plant'
            });
        }
    });
})


module.exports = router;
