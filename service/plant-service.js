var Plant = require('../model/plant').Plant;
var multer = require('multer');
var pather = require('path');
var PlantService = {};
var store = require('store');
var fs = require('fs');

PlantService.findPlant = function (plant_name, next) {
    Plant.findOne({ plant_name: plant_name }, function (err, plant) {
        return next(err, plant);
    });
}

function base64_encode(file) {
    // read binary data
    var bitmap = fs.readFileSync(file);
    // convert binary data to base64 encoded string
    return new Buffer(bitmap).toString('base64');
}

PlantService.addPlant = function (data, next) {

    
    this.findPlant(data.body.plant_name, function (err, plant) {
        if (err) {
            console.log('Encountered error when searching if the Plant is in the db already');
            return next(err, null);
        }

        if (plant) {
            console.log('Plant with name ' + plant.plant_name + ' exists already.');
            return next(null, null);
        }
        else {

            var storage = multer.diskStorage({
                destination:'./public/uploads/',
                filename: function(req, file, callback){
                    callback(null, file.fieldname + '_' + Date.now() + pather.extname(file.originalname));
                }
            })
            
            var upload = multer({ storage:storage }).single('file');   
            
        //    console.log(data.req.body.req)

            upload(data, data.res, function(err){
                if(err){
                    console.log(err);
                }else{
                    if(data.file != undefined){
                        console.log(data.body);
                        console.log(data.body.data.plant_name)
                        var base64str = base64_encode(data.file.path);
                        var imgSrc = 'data:image/png;base64,' + base64str;
                        // console.log(" base " +base64str)
                               /*Add Plant to db*/
                        var newPlant = new Plant({
                            plant_name: data.body.data.plant_name,
                            plant_disease_name:  data.body.data.plant_disease_name,
                            plant_disease_sympton:  data.body.data.plant_disease_sympton,
                            plant_disease_image: imgSrc,
                            plant_disease_description:  data.body.data.plant_disease_description,
                        });

                        newPlant.save(function (err, plant) {
                            return next(err, plant);
                        })

                    }
                   
           
                }
            })
        
     
        }
    })
}

PlantService.checkPlant = function (data, next) {
    this.findPlant(data.Plantname, function (err, Plant) {
        if (err) {
            console.log('Encountered error when searching if the Plant is in the db already');
            return next(err, null);
        }

        if (Plant) {
            console.log('Plant with merchantId ' + Plant.Plantname + ' exists already.');
            return next(null, null);
        }
        else {
            /*Add Plant to db*/
            var newPlant = new Plant({
                Plantname: data.Plantname,
                fullname: data.fullname,
                email: data.email,
                password: data.password
            });

            newPlant.save(function (err, Plant) {
                return next(err, Plant);
            })
        }
    })
}

PlantService.allPlants = function (next) {
    Plant.find(function (err, plants) {
        return next(err, plants);
    });
}

PlantService.countAll = function (next) {
    Plant.count(function (err, plants) {
        console.log(plants);
        return next(err, plants);
    });

}

PlantService.findById = function (id , next) {
    Plant.findOne({ "_id": id }, function (err, plant) {
        return next(err, plant);
    });
}

PlantService.deletePlant = function (id, next) {

    Plant.remove({ "_id": id }, function (err) {
        return next(err);
    });
}

PlantService.updatePlant = function (data, next) {
    console.log(data);
    console.log(data.plant_name);
    Plant.update({ "_id": data._id }, { $set: { "plant_name": data.plant_name, "plant_disease": data.plant_disease, "plant_disease_sympton": data.plant_disease_sympton, "plant_disease_description": data.plant_disease_description, "plant_disease_image": data.plant_disease_image } }, function (err, plant) {
        return next(err, plant);
    }, { multi: true });
}

module.exports = PlantService;